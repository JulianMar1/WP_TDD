<?php

/**
 * Class The_content_filter
 *
 * @package Wp_Tdd
 */

namespace wp_tdd\Test;

use wp_tdd\WP_TDD as wp_tdd;

class WP_TDD_Test extends \WP_UnitTestCase {

	private $wptdd;

	private $string_to_add = 'add this to the content';

	public function setup() {

		parent::setUp();

		$this->wptdd = new WP_TDD( $this->string_to_add );

	}

	public function test_set_add_to_content() {

		$expected = "add this to the content";

		$this->wptdd->set_string_to_content( $expected );

		$result = $this->wptdd->get_string_to_content();

		$this->assertEquals( $expected, $result );

	}

	public function test_wp_tdd() {

		$expected = "I am the content of a post";
		// $add_to_the_content = "I was added to the post";

		$post = $this->factory->post->create_and_get(array(
			'post_content' => $expected,
		));

		$content = $post->post_content;

		$result = apply_filters( 'the_content', $content );

		$result = trim( preg_replace( '/\s+/', ' ', $result ) );

		$this->assertEquals( "<p>{$content}</p> {$this->string_to_add}", $result );
	}
}
