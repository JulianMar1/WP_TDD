<?php
/**
 * Plugin Name:     Wp Tdd
 * Plugin URI:      PLUGIN SITE HERE
 * Description:      lugin to demonstrate TDD in wordpress
 * Author:          Julian Martin
 * Author URI:      YOUR SITE HERE
 * Text Domain:     wp-tdd
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Wp_Tdd
 */

namespace wp_tdd;

class WP_TDD {

	private $string_to_content;

	public function __construct( $string ) {
		$this->set_string_to_content( $string );
		add_filter( 'the_content', array( $this, 'add_to_content' ), 10, 1 );
	}

	public function add_to_content( $content ) {

		return $content . $this->get_string_to_content();
	}

	public function set_string_to_content( $string ) {
		$this->string_to_content = $string;
	}

	public function get_string_to_content() {
		return $this->string_to_content;
	}
}
